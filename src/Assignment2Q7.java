import java.util.Scanner;


public class Assignment2Q7 {

	/**
	 * Convert a decimal number to binary
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int base10,base2,length,r,count,number;
		base2=0;
		Scanner sc= new Scanner(System.in);
		System.out.print("Enter the decimal number: ");
		base10=sc.nextInt();
		number=base10;
		count=0;
		while(base10*2!=0){
			r=base10%2;
			base10=base10/2;
			base2=(int) (r*Math.pow(10, count))+base2;
			count++;
		}
		System.out.println("The binary representation of "+number+" is "+base2);
	}

}
