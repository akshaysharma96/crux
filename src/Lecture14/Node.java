package Lecture14;

public class Node<T> {
	T data;
	Node <T> next;  //<T> so that the next variable points to the same type of node

	public Node(T data){
		this.data=data;
	}
}
