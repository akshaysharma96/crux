package Assignment4;

public class Assignment4Q5 {

	/**
	 * Sort an array of strings based on length
	 */
	public static void sortStringArray(String array[]){
		int j;
		for(int i=1;i<array.length;i++){
			j=i-1;
			String key=array[i];
			while(j>=0&&array[j].length()>key.length()){
				array[j+1]=array[j];
				j--;
			}
			array[j+1]=key;
		}
	}
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		String array[]={"hello","str","a","askljlsdf","aa","lo"};
		sortStringArray(array);
		for(int i=0;i<array.length;i++){
			System.out.println(array[i]);
		}
	}

}
