package Assignment4;

public class SelectionSort {

	/**
	 * @param args
	 */
	public static void selectionSort(int a[]){
		for(int i=0;i<a.length-1;i++){
			int key=i;
			for(int j=i+1;j<a.length;j++){
				if(a[j]<a[key]){
					key=j;
				}
			}
			if(key!=i){
				int temp=a[key];
				a[key]=a[i];
				a[i]=temp;
			}
		}
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int a[]={5,4,3,2,1};
		selectionSort(a);
		for(int i=0;i<a.length;i++){
			System.out.println(a[i]);
		}
	}

}
