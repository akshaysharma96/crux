package Lecture3;

public class NegativePositiveSum {

	/**
	 * 
	 * Print sum of negative and positive numbers separately in an array. 
	 */
	public static void returnSum(int [] array){
		int positiveSum=0;
		int negativeSum=0;
		for(int i=0;i<array.length;i++){
			if(array[i]>=0){
				positiveSum=positiveSum+array[i];
			}
			else{
				negativeSum=negativeSum+array[i];
			}
		}
		System.out.println("The sum of postive numbers is: "+positiveSum+" and the sum of all negative numbers is: "+negativeSum);
	}
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int[] array={1,23,4,5,6,-9,-23,45,-56};
		returnSum(array);
	}

}
