package Lecture3;

public class Assignment3Q6 {

	/**
	 * You are given two arrays. Find the sum of the two arrays and put the result in
	 * another array. e.g. if you are given [1,2,4] and [4,5,6] the output should be
	 * [5,8,0].
	 */
	public static void printSum(int[] array1, int[] array2){
		if(array1.length==0||array2.length==0){
			System.out.println("Arrays are empty");
			return;
		}
		else if(array1.length!=array2.length){
			System.out.println("Lengths of the arrays are not equal");
			return;
		}
		int[] result= new int[array1.length];
		for(int i=0;i<array1.length;i++){
			result[i]=array1[i]+array2[i];
		}
		System.out.println("The resultant arrays is: ");
		for(int i=0;i<result.length;i++){
			System.out.print(result[i]+" ");
		}
	}
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int[] array1={1,24};
		int[] array2={35,0};
		printSum(array1, array2);
	}

}
