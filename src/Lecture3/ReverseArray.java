package Lecture3;

public class ReverseArray {

	/**
	 * Reverse an array,without using extra spaces
	 */
	
	public static void reverseArray(int []array){
		int l=array.length-1;
		
		for(int i=0;i<=l/2;i++){
			int last=array[l-i];
			array[l-i]=array[i];
			array[i]=last;
		}
		/*
		 * No need to return array, as memory address is passed to function
		 * and changes will reflect in the actual array
		 * */
		//return array;
	}
	/**
	 * Prints an array
	 */
	public static void printArray(int[]array){
		for(int i=0;i<array.length;i++){
			System.out.print(array[i]+" ");
		}
	}
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int[] testArray={1,2,3,4,5};
		reverseArray(testArray);
		printArray(testArray);
	}

}
