package Lecture3;

public class Assignment3Q5 {

	/**
	 * Given an array of integers A and an integer x. Find pairs of elements in A which
	 * sum to x
	 */
	
	public static void findPairs(int []array,int number){
		for(int i=0;i<array.length-1;i++){
			for(int j=i+1;j<array.length;j++){
				if(array[i]+array[j]==number){
					System.out.println("("+array[i]+","+array[j]+")");
				}
			}
		}
	}
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int[] array={0,1,2,3,4,5};
		int number=4;
		findPairs(array, number);
	}

}
