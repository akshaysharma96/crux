package Lecture3;

public class RootWithPrecision {

	/**
	 * Print square root of a number with a precision upto particular decimal
	 * points
	 */
	public static double getSquareRoot(double number,int precision){
		double increment=1.0;
		int decimalNumber=0;
		double output=1.0;
		while(decimalNumber<=precision){
			while(output*output<=number){
				output=output+increment;
				//System.out.println(output);
			}
			output=output-increment;
			increment=increment*0.1;
			decimalNumber++;
		}
		
		return output;
	}
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		double result=getSquareRoot(167, 5);
		System.out.println(result);
		
	}

}
