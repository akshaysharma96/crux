package Lecture11;

public class ProductUse {

	/**
	 * OOPS Lecture 1
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Product p1= new Product();
		p1.name="iPhone";
		p1.price=50000;
		//changed for p1, reflects for the whole class, as it's static
		
		//no_product=99;
		Product.no_product--;				//the value is now same for all objects
		
		/*
		 * We should not access static member, by a non static way i.e using objects.
		 * As they are static therefore they static members must be accessed using class
		 * */

		System.out.println(p1.name+" price: "+p1.price+" no of product: "+Product.no_product);
		
		
		Product p2= new Product();
		p2.name="Samsung Galaxy S3";
		p2.price=45890;
		
		//no_product=98;
		Product.no_product--;


		System.out.println(p2.name+" price: "+p2.price+" no of product: "+Product.no_product);
	}

}
