import java.util.Scanner;


public class Assingment1Pattern6 {

	/**
	 * @param args
	 */
	/*Display pattern like:
	 * 5
	 * 44
	 * 333
	 * 2222
	 * 11111
	 * 
	 * 
	 * */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int N;
		Scanner sc=new Scanner(System.in);
		System.out.print("Enter the number of rows: ");
		N=sc.nextInt();
		int rows=1;
		while(rows<=N){
			int col=1;
			int number=N-rows+1;
			while(col<=rows){
				System.out.print(number);
				col++;
			}
			rows++;
			System.out.println();
		}
	}

}
