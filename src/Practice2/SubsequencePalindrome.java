package Practice2;

public class SubsequencePalindrome {

	/**
	 * Find the subsequences of a string that are palindromes
	 */
	public static void findPalindrome(String str){
		int left;int right;
		for(int i=0;i<str.length();i++){
			System.out.print(str.charAt(i)+" ");
			left=i-1;
			right=i+1;
			while((left>=0&&right<str.length())&&str.charAt(left)==str.charAt(right)){
				System.out.print(str.substring(left,right+1)+" ");
				left--;
				right++;
			}
		}
		for(int j=0;j<str.length()-1;j++){
			if(str.charAt(j)==str.charAt(j+1)){
				System.out.print(str.substring(j,j+2)+" ");
				left=j-1;
				right=j+2;
				while((left>=0&&right<str.length())&&str.charAt(left)==str.charAt(right)){
					System.out.print(str.substring(left,right+1)+" ");
					left--;
					right++;
				}
			}
		}
	}
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		findPalindrome("aabaa");
	}

}
