package Practice2;

public class FindGPOptimized {

	/**
	 * Find the sum of a G.P using recursion with optimization
	 */
	public static double findSum(double r, int n){
		if(n==1){
			return r;
		}
		return (1+r*(findSum(r, n-1)));
	}
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.out.println(findSum(0.5, 20));
	}

}
