import java.util.Scanner;


public class PrintFib {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int N;
		Scanner sc= new Scanner(System.in);
		System.out.print("Enter number: ");
		N=sc.nextInt();
		int sum=0;
		int i=0;
		int j=1;
		while(sum<=N){
			System.out.println(sum);
			sum=i+j;
			i=j;
			j=sum;
		}
	}

}
