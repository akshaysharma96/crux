
public class Assignment2Q5 {

	/**
	 * @param args
	 */
	/*
	 * Print first 20 terms of the series ((3*n)+2), that are not divisible by 4
	 * */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int seriesMember;
		int count=0;
		while(count<20){
			seriesMember=(3*count+2);
			if(seriesMember%4!=0){
				System.out.print(seriesMember+" ");
			}
			count++;
		}
	}

}
