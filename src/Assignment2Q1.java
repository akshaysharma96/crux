import java.util.Scanner;


public class Assignment2Q1 {

	/**
	 * @param args
	 */
	/*
	 * Gives choice to the user to get the sum of 1 to n, or product from 1 to n 
	 * */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int N;
		Scanner sc = new Scanner(System.in);
		System.out.print("Enter the number: ");
		N=sc.nextInt();
		sc.nextLine();
		String choice;
		System.out.print("Enter your choice: ");
		choice=sc.nextLine();
		if(choice.equals("sum")){
			int sum=N*(N+1);
			System.out.print("Sum of numbers from 1 to "+N+" is: "+sum/2);
		}
		else if(choice.equals("product")){
			int product=1;
			int count=1;
			while(count<=N){
				product=product*count;
				count++;
			}
			System.out.println("The product of numbers from 1 to "+N+" is: "+product);
		}
		else{
			System.out.println("Invalid input");
		}
		
	}

}
