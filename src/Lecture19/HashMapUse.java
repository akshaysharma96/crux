package Lecture19;

import java.util.HashMap;

public class HashMapUse {

	public static void printIntersection(int[] a, int [] b){
		HashMap<Integer, Integer> map =  new HashMap<Integer, Integer>();
		//		for(int i: a){				//i ke andr value rather than index
		//			map.put(arg0, arg1)
		//		}
		for(int i=0;i<a.length;i++){
			if(map.containsKey(a[i])){
				int oldValue=map.get(a[i]);
				oldValue++;
				map.put(a[i],oldValue);
			}
			else{
				map.put(a[i], 1);
			}
		}

		//find intersection
		for(int i=0;i<b.length;i++){
			if(map.containsKey(b[i]) && map.get(b[i]) > 0){
				System.out.print(b[i]+" ");
				map.put(b[i],map.get(b[i])-1);
			}
		}
	}
	
	public static int[] removeDuplicates(int[] a){
		if(a.length==0||a.length==1)
			return a;
		
		HashMap<Integer,Integer> map  = new HashMap<Integer, Integer>();
		
		for(int i=0;i<a.length;i++){
			if(!map.containsKey(a[i])){
				map.put(a[i], 0);
			}
			else{
				a[i]=0;
			}
		}
		
		return a;
	}
	
	
	public static void sumToZero(int [] arr){
		if(arr.length==0||arr.length==1)
			return;
		HashMap<Integer,Integer> map  = new HashMap<Integer, Integer>();
		
		for(int i=0;i<arr.length;i++){
			if(map.containsKey(arr[i])){
				int oldValue=map.get(arr[i]);
				oldValue++;
				map.put(arr[i], oldValue);
			}
			else{
				map.put(arr[i], 1);
			}
		}
	}
	
	public static int[] removeDuplicates2(int[] a){
		if(a.length==0||a.length==1)
			return a;
		HashMap<Integer,Integer> map  = new HashMap<Integer, Integer>();
		for(int i=0;i<a.length;i++){
			map.put(a[i], 1);
		}
		int[] result = new int[map.size()];
		int j=0;
		for(int i=0;i<a.length;i++){
			if(map.containsKey(a[i])){
				result[j]=a[i];
				map.remove(a[i]);
				j++;
			}
		}
		return result;
	}

	public static void main(String[] args) {
		// <K= key type, V= value type>
//		HashMap<String,Integer> map = new HashMap<String, Integer>();
//		map.put("abc",1);
//		map.put("def", 3);
//		System.out.println(map.get("abc"));
//		map.put("abc", 10);
//		System.out.println(map.get("abc"));
//		System.out.println(map.containsKey("lko"));
//		int a[]={1,2,4,5,6,7,8,6,4};
//		int b[] ={4,4,5,6,7,7,6,7,7};
//		printIntersection(a, b);
//		int[] arr ={1,3,1,5,6,7,3,9,5,9,7,4,7};
//		arr=removeDuplicates2(arr);
//		for(int i:arr){
//			System.out.print(i+" ");
//		}
		int a[] = {2,-2,2,-2};
		sumToZero(a);
	}


}
