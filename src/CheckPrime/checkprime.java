import java.util.*;
//Class 1 ,date: 16-01-2016

/*
    Checks whether a number is prime or not.
*/

class CheckPrime{
    public static void main(String[]args){
        int number;
        Scanner sc= new Scanner(System.in);
        System.out.print("Input number: ");
        number=sc.nextInt();
        int i=2;
        while(i<number/2){
            if(number%i==0){
                System.out.println(number+" is not prime");
                System.exit(0);
            }
            i=i+1;
        }
        System.out.println(number+" is prime");
    }
}