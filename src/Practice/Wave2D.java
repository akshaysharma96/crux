package Practice;

public class Wave2D {

	/**
	 * Print a 2D array
	 * 
	 */
	public static void printWave(int array[][]){
		for(int j=0;j<array.length;j++){
			if(j%2==0){
				for(int i=0;i<array[j].length;i++){
					System.out.print(array[i][j]+" ");
				}
			}
			else{
				for(int i=array[j].length-1;i>=0;i--){
					System.out.print(array[i][j]+" ");
				}
			}
		}
	}
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int a[][]={{1,2,3,4},{5,6,7,8},{9,10,11,12},{13,14,15,16}};
		printWave(a);
	}

}
