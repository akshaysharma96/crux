package Practice;

public class SelectionSortRecursion {

	/**
	 *Carry out selection sort recursively
	 */
	public static void selectionSort(int a[],int firstIndex){
		if(firstIndex>=a.length){
			return;
		}
		else{
			int key=firstIndex;
			for(int i=firstIndex+1;i<a.length;i++){
				if(a[key]>a[i]){
					key=i;
				}
			}
			int temp=a[firstIndex];
			a[firstIndex]=a[key];
			a[key]=temp;
			
			selectionSort(a, firstIndex+1);
		}
	}
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int [] a ={45,1,34,12,7,90,123};
		selectionSort(a, 0);
		for(int i=0;i<a.length;i++){
			System.out.print(a[i]+" ");
		}
	}

}
