package Recursion3;

public class Q4 {

	/**
	 * (()) check if there exist even number of parenthesis in a string
	 */
	public static boolean checkParenthesis(String str,int startIndex, int lastIndex){
		if(str.length()%2!=0){
			return false;
		}
		if(startIndex>=lastIndex){
			return true;
		}
		if(str.charAt(startIndex)=='('&&str.charAt(lastIndex)==')')
			return checkParenthesis(str, startIndex+1, lastIndex-1);
		
		else
			return false;
	}
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.out.println(checkParenthesis("(((x)))",0, 5));
	}

}
