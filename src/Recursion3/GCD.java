package Recursion3;

public class GCD {

	/**
	 *Find the greatest common divisor of two numbers
	 */
	public static int HCF(int a,int b){
		int big,small;
		if(b>a){
			big=b;
			small=a;
		}
		else{
			big=a;
			small=b;
		}
		if(big%small==0){
			return small;
		}
		else{
			return HCF(small,big%small);
		}
	}
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int hcf=HCF(7,12);
		System.out.println(hcf);
	}

}
