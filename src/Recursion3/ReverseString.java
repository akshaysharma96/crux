package Recursion3;

public class ReverseString {

	/**
	 * Reverse a string, using recursion
	 * 
	 */
	public static String reverseString(String str, int startIndex){
		if(startIndex==str.length()-1){
			return String.valueOf(str.charAt(startIndex));
		}
		
		String s =reverseString(str, startIndex+1);
		return s+String.valueOf(str.charAt(startIndex));
		
	}
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		String s =reverseString("sdhlsdj",0);
		System.out.println(s);
	}

}
