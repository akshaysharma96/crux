package Recursion3;

public class MinElementArray {

	/**
	 * Find the minimum element in an array.
	 */
	
	public static int smallestElement(int a[],int firstIndex){
		if(firstIndex==a.length-1){
			return a[firstIndex];
		}
		
		int nextElement=smallestElement(a, firstIndex+1);
		int element;
		if(nextElement<a[firstIndex])
			element=nextElement;
		else
			element=a[firstIndex];
		
		return element;
	}
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int a[] ={78,14,12};
		int element=smallestElement(a, 0);
		System.out.println(element);
	}

}
