import java.util.Scanner;


public class AssignmentPattern1 {

	/**
	 * @param args
	 */
	
	/*
	 * Display pattern : 
	 * 1
	 * 11
	 * 111
	 * 1111
	 * 11111
	 * .......
	 * 
	 * */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int N;
		Scanner sc= new Scanner(System.in);
		System.out.print("Enter the numbe of rows: ");
		N=sc.nextInt();
		int rows=1;
		while(rows<=N){
			int col=1;
			while(col<=rows){
				System.out.print(1);
				col++;
			}
			rows++;
			System.out.println();
		}
	}

}
