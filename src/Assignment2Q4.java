import java.util.Scanner;


public class Assignment2Q4 {

	/**
	 * @param args
	 */
	
	/*Program to generate the reverse of a number
	 *
	 * */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int N,length;
		Scanner sc= new Scanner(System.in);
		System.out.print("Enter the number: ");
		N=sc.nextInt();
		length=String.valueOf(N).length();
		int count,reverseNum,q,number;
		number=N;
		count=0;
		reverseNum=0;
		while(length>0){
			q=(int) (number/Math.pow(10,length-1));
			number=(int) (number%Math.pow(10,length-1));
			reverseNum=q*(int)Math.pow(10,count)+(reverseNum);
			count++;
			length--;
		}
		
		System.out.println("The reverse of the input: "+N+" is: "+reverseNum);
	}

}
