import java.util.Scanner;


public class Assignment1Pattern4 {

	/**
	 * @param args
	 */
	/*
	 * 	Display pattern 
	 * 1
	 * 11
	 * 202
	 * 3003
	 * 40004
	 * ......
	 * */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int N;
		Scanner sc=new Scanner(System.in);
		System.out.print("Enter the number of rows: ");
		N=sc.nextInt();
		int rows=1;
		while(rows<=N){
			if(rows==1){
				System.out.print(1);
			}
			else{
				int col=1;
				while(col<2){
					System.out.print(rows-1);
					col++;
				}
				col=1;
				while(col<=rows-2){
					System.out.print(0);
					col++;
				}
				col=1;
				while(col<2){
					System.out.print(rows-1);
					col++;
				}
			}
			rows++;
			System.out.println();
		}
		
	}

}
