package Lecture10;

public class QuickSort {

	/**
	 * Quicksort
	 */

	public static void quicksort(int a[], int startIndex, int endIndex){
		if(startIndex >= endIndex){
			return;
		}

		int pivot=partititon(a, startIndex,endIndex);
		quicksort(a, startIndex, pivot-1);
		quicksort(a, pivot+1, endIndex);
	}

	public static int partititon(int[] a, int startIndex, int endIndex) {
		int count=0;
		int pivot=a[startIndex];
		for(int i=startIndex+1;i<=endIndex;i++){
			if(a[i]<pivot){
				count++;
			}
		}
		int pivotIndex=startIndex+count;
		a[startIndex]=a[pivotIndex];
		a[pivotIndex]=pivot;
		
		//Rearrange
		int i=startIndex;
		int j=endIndex;
		while(i<j){
			while(i<=endIndex && a[i]<=pivot){
				i++;
			}
			while(j>=startIndex && a[j] > pivot){
				j--;
			}
			if(i<=j){
				int temp=a[i];
				a[i]=a[j];
				a[j]=temp;
				i++;
				j--;
			}
			
		}
		return pivotIndex;
		
	}
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int[] a ={6,3,2,12,56,1,4};
		quicksort(a,0,a.length-1);
		for(int i=0;i<a.length;i++){
			System.out.print(a[i]+" ");
		}
	}

}
