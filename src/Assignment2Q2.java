import java.util.Scanner;


public class Assignment2Q2 {

	/**
	 * @param args
	 */
	/*Calculate the sum of odd terms and even terms in a number
	 * 
	 * */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int N,length;
		Scanner sc = new Scanner(System.in);
		System.out.print("Enter the number: ");
		N=sc.nextInt();
		sc.nextLine();
		length=String.valueOf(N).length();
		int odd_sum,even_sum;
		even_sum=0;
		odd_sum=0;
		int q,number;
		number=N;
		while(length>0){
			q=(int) (number/(Math.pow(10,length-1)));
			number=(int) (number%(Math.pow(10,length-1)));
			if(q%2==0){
				even_sum=even_sum+q;
			}
			else{
				odd_sum=odd_sum+q;
			}
			length--;
		}
		System.out.println("Sum of odd terms: "+odd_sum+" sum of even terms: "+even_sum);
	}

}
