import java.util.*;

//Class 1, dated :16-01-2016

/*
    Generates Pattern like
    
        1
       232
      34543
     4567654

Hint: Here the forward spaces are equal to the Number of rows fiven by user - the current row number, back spaces will come automatically as the pattern will be generated
*/


class Pattern{
    public static void main(String[]args){
        int N;
        Scanner sc= new Scanner(System.in);
        System.out.print("Enter rows: ");
        N=sc.nextInt();
        int rows=1;
        int value;
        while(rows<=N){
            int spaces=1;
            while(spaces<=N-rows){
                System.out.print(" "); 
                spaces++;
            }
            value=rows;
            int col=1;
            while(col<=rows){
                System.out.print(value);
                value++;
                col++;
            }
            col=1;
            
            while(col<rows){
                value--;
                System.out.print(value);   
                col++;
            }
            System.out.print("\n");
            rows++;
        }
    }
}