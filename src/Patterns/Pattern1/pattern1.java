import java.util.*;
//Class 1, dated : 16-01-2016
/*
    Generate pattern like 
    
    1
    2 3
    4 5 6
    7 8 9 10

*/

class Pattern{
    public static void main(String[]args){
        int N;
        System.out.print("Enter rows: ");
        Scanner sc= new Scanner(System.in);
        N=sc.nextInt();
        int value=1;
        int row=1;
        while(row <=N){
            int col=1;
            while(col<=row){
                System.out.print(value+" ");
                value++;
                col++;
            }
            System.out.print("\n");
            row++;
        }
    }
}