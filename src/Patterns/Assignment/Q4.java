import java.util.*;
// Class 1, dated : 16-01-2016


/*
    Class that generates Pattern:
        1
       101
       1001
       ........
*/

class Q4A{
    public static void main(String[]args){
        int N,spaces,col;
        int rows=1;
        Scanner sc= new Scanner(System.in);
        System.out.print("Enter rows: ");
        N=sc.nextInt();
        while(rows<=N){
            spaces=1;
            while(spaces<=N-rows){
                System.out.print(" ");
                spaces++;
            }
            if(rows==1){
                System.out.print(rows);
            }
            else{
                int number=(int)Math.pow(10,rows-1)+1;
                System.out.print(number);
            }
            System.out.print("\n");
            rows++;
        }
    }
}