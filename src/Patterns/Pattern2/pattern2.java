import java.util.*;
//Class 1, dated : 16-01-2016
/*
    Generate pattern like
    
         1
       2 3
     4 5 6
   7 8 9 10

Hint: Here the forward spaces are equal to the Number of rows fiven by user - the current row number

*/


class Pattern{
    public static void main(String[]args){
        int N;
        Scanner sc= new Scanner(System.in);
        int rows=1;
        int value=1;
        System.out.print("Enter rows: ");
        N=sc.nextInt();
        while(rows<=N){
            int spaces=1;
            while(spaces<=N-rows){
                System.out.print(" ");
                spaces++;
            }
            int col=1;
            while(col<=rows){
                System.out.print(value);
                value++;
                col++;
            }
            System.out.print("\n");
            rows++;
        }
    }
}