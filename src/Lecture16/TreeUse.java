package Lecture16;

import java.util.Scanner;

import Lecture15Queue.QueueEmptyException;
import StackAndQueues.Queue;

public class TreeUse {
	
	public static TreeNode<Integer> leveWiseINput() throws QueueEmptyException{
		Queue<TreeNode<Integer>> q= new Queue<TreeNode<Integer>>();
		Scanner sc = new Scanner(System.in);
		System.out.print("Enter the value of root node: ");
		int data=sc.nextInt();
		TreeNode<Integer> root = new TreeNode<Integer>(data);
		q.enqueue(root);
		while(!q.isEmpty()){
			TreeNode<Integer> dequeued = q.dequeue();
			System.out.print("Enter the number of children of "+dequeued.data+": ");
			int root_number=sc.nextInt();
			for(int i=0;i<root_number;i++){
				System.out.print("Enter the "+i+"th child of "+dequeued.data+": ");
				int data_value=sc.nextInt();
				TreeNode<Integer> node=new TreeNode<Integer>(data_value);
				q.enqueue(node);
				dequeued.children.add(node);
			}
		}
		sc.close();
		return root;
	}
	
	public static void printTree(TreeNode<Integer> root){
		String str=root.data+":";
		for(TreeNode<Integer> child:root.children){
			str=str+child.data+",";
		}
		System.out.println(str);
		for(TreeNode<Integer> child:root.children)
			printTree(child);
	}
	
	public static void levelWisePrint(TreeNode<Integer> root) throws QueueEmptyException{
		Queue<TreeNode<Integer>> q = new Queue<TreeNode<Integer>>();
		q.enqueue(root);
		while(!q.isEmpty()){
			TreeNode<Integer> dequequed=q.dequeue();
			String str=dequequed.data+": ";
			for(TreeNode<Integer>child :dequequed.children){
				q.enqueue(child);
				str=str+child.data+",";
			}
			System.out.print(str);
			System.out.println();
		}
	}
	
	public static int getNumberOfNodes(TreeNode<Integer> root){
		if(root==null){
			return 0;
		}
		int number=1;
		for(TreeNode<Integer> child:root.children){
			number=number+getNumberOfNodes(child);
		}
		return number;
	}
	
	public static int sumOfNodes(TreeNode<Integer> root){
		if(root==null){
			return 0;
		}
		int sum=root.data;
		for(TreeNode<Integer> child:root.children){
			sum=sum+sumOfNodes(child);
		}
		return sum;
	}
	
	public static int getHeight(TreeNode<Integer> root){
		if(root==null){
			return 0;
		}
		int height=0;
		for(TreeNode<Integer> child:root.children){
			int ans=getHeight(child);
			if(ans>height)
				height=ans;
		}
		
		return height+1;
	}
	
	public static void nodeAtDepthK(TreeNode<Integer> root,int k){
		if(root==null){
			return;
		}
		
		if(k==0){
			System.out.print(root.data+" ");
			return;
		}
		for(TreeNode<Integer> child:root.children){
			nodeAtDepthK(child, k-1);
		}
	}
	
	
	public static int  maxNode(TreeNode<Integer> root){
		if(root==null){
			return 0;
		}
		int max=root.data;
		for(TreeNode<Integer> child:root.children){
			int ans=maxNode(child);
			if(ans>max)
				max=ans;
		}
		
		return max;
	}
	
	public static int nodesGreaterThanK(TreeNode<Integer> root, int k){
		if(root==null){
			return 0;
		}
		int number=0;
		if(root.data>k){
			number=1;
		}else{
			number=0;
		}
		
		for(TreeNode<Integer> child:root.children){
			number=number+nodesGreaterThanK(child, k);
		}
		return number;
	}
	
	public static int largestSumOfNodes(TreeNode<Integer> root){
		if(root==null){
			return 0;
		}
		int sum=root.data;
		for(TreeNode<Integer> child:root.children)
			sum=sum+child.data;
		for(TreeNode<Integer> child:root.children){
			int ans=largestSumOfNodes(child);
			if(ans>sum)
				sum=ans;
		}
		
		return sum;
	}
	public static void main(String[]args) throws QueueEmptyException{
		TreeNode<Integer> root =leveWiseINput();
		//printTree(root);
		//levelWisePrint(root);
		//System.out.println(getNumberOfNodes(root));
		//System.out.println(sumOfNodes(root));
		//System.out.println(getHeight(root));
		//nodeAtDepthK(root, 1);
		//System.out.println(maxNode(root));
		//System.out.println(nodesGreaterThanK(root, 0));
		System.out.println(largestSumOfNodes(root));
	}
}
