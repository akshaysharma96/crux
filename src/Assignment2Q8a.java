import java.util.Scanner;


public class Assignment2Q8a {

	/**
	 * @param args
	 * 
	 * Calculate the root of a number, with only integral part
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
			double N;
			Scanner sc= new Scanner(System.in);
			System.out.print("Enter the number: ");
			N=sc.nextDouble();
			sc.nextLine();
			double count=0;
			while(N-(count*count)>=0.0000001){
				count=count+0.000001;
			}
			int integer_part=(int)count;
			System.out.println(integer_part);
			
	}

}
