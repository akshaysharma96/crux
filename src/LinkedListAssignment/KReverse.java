package LinkedListAssignment;

import java.util.Scanner;
/**
 * 
 * Program to K reverse a linked list
 * 
 * */
public class KReverse {
	public static Node<Integer> takeInput() {
		Scanner s = new Scanner(System.in);
		Node<Integer> head = null, tail = null;
		System.out.println("Enter next node : ");
		int data = s.nextInt();
		while(data != -1) {
			Node<Integer> newNode = new Node<Integer>(data);
			if(head == null) {
				head = newNode;
				tail = newNode;
			}
			else {
				tail.next = newNode;
				tail = newNode;
			}
			System.out.println("Enter next node : ");
			data = s.nextInt();
		}
		return head;
	}

	public static void printLL(Node<Integer> head){
		if(head==null)
			return;
		Node<Integer> temp=head;
		while(temp!=null){
			System.out.print(temp.data+"->");
			temp=temp.next;
		}
		System.out.println("");
	}
	
	public static Node<Integer> kReverse(Node<Integer> head, int k){
		Node<Integer> temp=head,previous=null,nexNode=null;
		int i=0;
		while(i<k){
			if(temp==null){
				return previous;
			}
			nexNode=temp.next;
			temp.next=previous;
			previous=temp;
			temp=nexNode;
			i++;
		}
		
		head.next=kReverse(nexNode,k);
		return previous;
	}
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Node<Integer> head=takeInput();
		printLL(head);
		head=kReverse(head, 5);
		printLL(head);
	}

}
