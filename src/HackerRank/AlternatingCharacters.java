package HackerRank;

/*
 Shashank likes strings in which consecutive characters are different. For example, he likes ABABA, while he doesn't like ABAA. Given a string containing characters AA and BB only, he wants to change it into a string he likes. To do this, he is allowed to delete the characters in the string.

Your task is to find the minimum number of required deletions.

Input Format

The first line contains an integer TT, i.e. the number of test cases. 
The next TT lines contain a string each.

Output Format

For each test case, print the minimum number of deletions required.

Constraints

1≤T≤101≤T≤10 
1≤1≤ length of string ≤105
*/
public class AlternatingCharacters {
	public static void getIterations(String str){
		if(str.length()==0||str.length()==1){
			System.out.println(0);
			return;
		}
		int count=0;
		for(int i=0;i<str.length()-1;i++){
			if(str.charAt(i)==str.charAt(i+1))
				count++;
		}
		System.out.println(count);
		
	}
	
	public static void main(String[] args){
		getIterations("AAABBB");
	}
}
