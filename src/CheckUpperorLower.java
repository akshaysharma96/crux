import java.util.Scanner;


public class CheckUpperorLower {

	/**
	 * @param args
	 */
	/*
	 * Check whether a given input alphabet is UpperCase or LowerCase
	 * */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner sc=new Scanner(System.in);
		char alphabet;
		System.out.print("Enter the alphabet: ");
		alphabet=sc.next().charAt(0);
		int asciiValue=(int)alphabet;
		if(asciiValue>=65&&asciiValue<=90){
			System.out.println("Upper Case");
			return;
		}
		else if(asciiValue>=97&&asciiValue<=122){
			System.out.println("Lower case");
			return;
		}
		else{
			System.out.println(alphabet+" is not an alphabet");
			return;
		}
	}
	

}
