package Lecture15Queue;

public class Queue<T> {
	private Node<T> head;
	private Node<T> tail;
	private int size=0;

	public Queue(T data){
		Node<T> newNode=new Node<T>(data);
		head=newNode;
		tail=newNode;
		size=1;
	}

	public Queue(){
		head=null;
		tail=null;
		size=0;
	}

	public int size(){
		return size;
	}

	public void enqueue(T data){
		if(head==null){
			Node<T> node=new Node<T>(data);
			head=node;
			tail=node;
			size=1;
			return;
		}
		Node<T> newNode=new Node<T>(data);
		tail.next=newNode;
		tail=newNode;
		size++;
	}

	public T dequeque() throws QueueEmptyException{
		if(head==null){
			throw new QueueEmptyException();
		}
		T data=head.data;
		head=head.next;
		size--;
		return data;

	}
	
	public void showQueue(){
		Node<T> temp=head;
		while(temp!=null){
			System.out.println(temp.data);
			temp=temp.next;
		}
		System.out.println();
	}

	public T front() throws QueueEmptyException{
		if(head==null){
			throw new QueueEmptyException();
		}

		return head.data;
	}
	
	public void reverseQueue() throws QueueEmptyException{
		if(size==1){
			return;
		}
		T data=dequeque();
		reverseQueue();
		enqueue(data);
	}
}
