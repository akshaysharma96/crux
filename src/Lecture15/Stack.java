package Lecture15;

public class Stack<T> {
	private Node<T> head;
	private int size;

	public int size(){
		return size;
	}

	public void push(T data){
		if(head==null){
			Node<T> node = new Node<T>(data);
			head=node;
			size=1;
			return;
		}
		Node<T> node = new Node<T>(data);
		node.next=head;
		head=node;
		size++;
	}

	public boolean isEmpty(){
		return head==null;
	}

	public T pop() throws StackEmptyExcemption{
		if(head==null){
			throw new StackEmptyExcemption();
		}
		T value= head.data;
		head=head.next;
		size--;
		return value;
		
	}
	
	public T top() throws StackEmptyExcemption{
		if(head==null){
			throw new StackEmptyExcemption();
		}
		return head.data;
	}
}
