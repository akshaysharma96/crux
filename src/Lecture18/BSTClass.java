package Lecture18;

import java.util.LinkedList;
import java.util.Scanner;
import java.util.logging.Level;

import Lecture15Queue.QueueEmptyException;
import LinkedList.Node;
import StackAndQueues.Queue;
import BinaryTree.BinaryTree;

public class BSTClass {

	BinaryTree<Integer> root;
	int size;

	public static void levelWisePrint(BinaryTree<Integer> root) throws QueueEmptyException{
		if(root==null)
			return;
		Queue<BinaryTree<Integer>> q = new Queue<BinaryTree<Integer>>();
		q.enqueue(root);
		while(!q.isEmpty()){
			BinaryTree<Integer> dequequed=q.dequeue();
			String str=dequequed.data+":";
			if(dequequed.left!=null){
				str=str+dequequed.left.data+",";
				q.enqueue(dequequed.left);
			}
			if(dequequed.right!=null){
				str=str+dequequed.right.data+",";
				q.enqueue(dequequed.right);
			}
			System.out.print(str);
			System.out.println();
		}

	}

	public static BinaryTree<Integer> takeInput() throws QueueEmptyException{
		Scanner sc= new Scanner(System.in);
		Queue<BinaryTree<Integer>> q = new Queue<BinaryTree<Integer>>();
		System.out.print("Enter the value of the root node: ");
		int data=sc.nextInt();
		if(data==-1||data<0){
			return null;
		}
		BinaryTree<Integer> root=new BinaryTree<Integer>(data);
		q.enqueue(root);
		while(!q.isEmpty()){
			BinaryTree<Integer> dequequed=q.dequeue();
			int leftNode,rightNode;
			System.out.print("Enter the value of the left node of "+dequequed.data+": ");
			leftNode=sc.nextInt();
			if(leftNode!=-1&&leftNode>=0){
				BinaryTree<Integer> leftTree=new BinaryTree<Integer>(leftNode);
				dequequed.left=leftTree;
				q.enqueue(leftTree);
			}
			System.out.print("Enter the value of the right node of "+dequequed.data+": ");
			rightNode=sc.nextInt();
			if(rightNode!=-1&&rightNode>=0){
				BinaryTree<Integer> rightTree=new BinaryTree<Integer>(rightNode);
				dequequed.right=rightTree;
				q.enqueue(rightTree);
			}
		}
		return root;
	}

	public int getMax(BinaryTree<Integer> root){
		if(root==null)
			return Integer.MIN_VALUE;

		int data=root.data;
		int n1=getMax(root.left);
		int n2=getMax(root.right);

		return Math.max(n1,Math.max(data, n2));
	}

	public int getMin(BinaryTree<Integer> root){
		if(root==null)
			return Integer.MAX_VALUE;
		int data=root.data;
		int n1=getMin(root.left);
		int n2=getMin(root.right);

		return Math.min(n1, Math.min(n2,data));
	}

	public static boolean isBST(BinaryTree<Integer> root){

		if(root==null)
			return true;

		if(root.left==null&&root.right==null)
			return true;

		Triplet ans=getTriplet(root);
		
		return ans.isBST;

	}
	
	public static Triplet getTriplet(BinaryTree<Integer> root){
		if(root==null){
			Triplet triplet= new Triplet();
			triplet.isBST=true;
			triplet.max=Integer.MIN_VALUE;
			triplet.min=Integer.MAX_VALUE;
			return triplet;
		}
		
		Triplet t1= getTriplet(root.left);
		Triplet t2= getTriplet(root.right);
		
		Triplet ans = new Triplet();
		ans.min=Math.min(root.data, Math.min(t1.min,t2.min));
		ans.max=Math.max(root.data, Math.max(t1.max,t2.max));
		
		boolean isBST=false;
		if(t1.isBST&&t2.isBST){
			if(root.data>t1.max&&root.data<t2.min){
				isBST=true;
			}
		}
		ans.isBST=isBST;
		return ans;
	}

	public static Node<Integer> getLinkedList(BinaryTree<Integer> root){
		if(root==null){
			return null;
		}

		int data=root.data;
		Node<Integer> currentNode = new Node<Integer>(data);
		Node<Integer> leftNode=getLinkedList(root.left);
		Node<Integer> rightNode=getLinkedList(root.right);
		if(leftNode==null){
			currentNode.next=rightNode;
			return currentNode;
		}
		Node<Integer> temp=leftNode;
		while(temp.next!=null){
			temp=temp.next;
		}
		temp.next=currentNode;
		currentNode.next=rightNode;
		return leftNode;
	}

	public static void printLL(Node<Integer> head){
		if(head==null)
			return;
		Node<Integer> temp=head;
		while(temp!=null){
			System.out.print(temp.data+" ");
			temp=temp.next;
		}
	}

	public static BinaryTree<Integer> treeFromArray(int[]arr, int startIndex, int endIndex){
		if(startIndex>endIndex)
			return null;
		if(startIndex==endIndex){
			return new BinaryTree<Integer>(arr[startIndex]);
		}

		int mid=(endIndex+startIndex)/2;
		BinaryTree<Integer> root=new BinaryTree<Integer>(arr[mid]);
		root.left=treeFromArray(arr, startIndex, mid-1);
		root.right=treeFromArray(arr, mid+1, endIndex);

		return root;
	}

	public static void insertNode(BinaryTree<Integer> root, int value){
		if(root==null){
			root=new BinaryTree<Integer>(value);
			return;
		}
		if(value<root.data){
			if(root.left==null){
				BinaryTree<Integer> newNode = new BinaryTree<Integer>(value);
				root.left=newNode;
				return;
			}
			else{
				insertNode(root.left, value);
			}
		}
		else{
			if(root.right==null){
				BinaryTree<Integer> newNode = new BinaryTree<Integer>(value);
				root.right=newNode;
				return;
			}
			else{
				insertNode(root.right, value);
			}
		}
	}

	public static void findNode(BinaryTree<Integer> root, int data){
		if(root==null){
			System.out.println("Node not found!");
			return;
		}
		if(root.data==data){
			System.out.println("Node found with data: "+data);
			return;
		}
		
		if(data<root.data)
			findNode(root.left, data);
		else
			findNode(root.right, data);
	} 

	public static void main(String[]args) throws QueueEmptyException{
		BinaryTree<Integer> root=takeInput();
		//		//		BSTClass bst = new BSTClass();
		//		//		System.out.println(bst.isBST(root));
		//				Node<Integer> head=getLinkedList(root);
		//				printLL(head);
		//		int[] arr={1,2,3,4,5,6,7,8};
		//		BinaryTree<Integer> root2=treeFromArray(arr,0,arr.length-1);
		//		levelWisePrint(root2);
//		insertNode(root, 8);
//		levelWisePrint(root);
//		findNode(root, 19);
		System.out.println(isBST(root));


	}
}
