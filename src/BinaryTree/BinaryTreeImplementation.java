package BinaryTree;

import java.util.Scanner;

import org.omg.PortableInterceptor.INACTIVE;

import Lecture15Queue.QueueEmptyException;
import StackAndQueues.Queue;


public class BinaryTreeImplementation {

	static int preIndex=0;

	public static BinaryTree<Integer> takeInput() throws QueueEmptyException{
		Scanner sc= new Scanner(System.in);
		Queue<BinaryTree<Integer>> q = new Queue<BinaryTree<Integer>>();
		System.out.print("Enter the value of the root node: ");
		int data=sc.nextInt();
		if(data==-1||data<0){
			return null;
		}
		BinaryTree<Integer> root=new BinaryTree<Integer>(data);
		q.enqueue(root);
		while(!q.isEmpty()){
			BinaryTree<Integer> dequequed=q.dequeue();
			int leftNode,rightNode;
			System.out.print("Enter the value of the left node of "+dequequed.data+": ");
			leftNode=sc.nextInt();
			if(leftNode!=-1&&leftNode>=0){
				BinaryTree<Integer> leftTree=new BinaryTree<Integer>(leftNode);
				dequequed.left=leftTree;
				q.enqueue(leftTree);
			}
			System.out.print("Enter the value of the right node of "+dequequed.data+": ");
			rightNode=sc.nextInt();
			if(rightNode!=-1&&rightNode>=0){
				BinaryTree<Integer> rightTree=new BinaryTree<Integer>(rightNode);
				dequequed.right=rightTree;
				q.enqueue(rightTree);
			}
		}
		return root;
	}

	public static int getHeight(BinaryTree<Integer> root){
		if(root==null)
			return 0;

		return Math.max(getHeight(root.left),getHeight(root.right))+1;
	}

	//	public static void levelOrder(BinaryTree<Integer> root) throws QueueEmptyException{
	//		if(root==null)
	//			return;
	//		Queue<BinaryTree<Integer>> q = new Queue<BinaryTree<Integer>>();
	//		q.enqueue(root);
	//		while(!q.isEmpty()){
	//			BinaryTree<Integer> dequequed =q.dequeue();
	//			System.out.print(dequequed.data+" ");
	//			if(dequequed.left!=null){
	//				q.enqueue(dequequed.left);
	//				System.out.print(dequequed.left.data+" ");
	//			}
	//			if(dequequed.right!=null){
	//				q.enqueue(dequequed.right);
	//				System.out.print(dequequed.right.data+" ");
	//			}
	//		}
	//	} 

	public static void levelWisePrint(BinaryTree<Integer> root) throws QueueEmptyException{
		if(root==null)
			return;
		Queue<BinaryTree<Integer>> q = new Queue<BinaryTree<Integer>>();
		q.enqueue(root);
		while(!q.isEmpty()){
			BinaryTree<Integer> dequequed=q.dequeue();
			String str=dequequed.data+":";
			if(dequequed.left!=null){
				str=str+dequequed.left.data+",";
				q.enqueue(dequequed.left);
			}
			if(dequequed.right!=null){
				str=str+dequequed.right.data+",";
				q.enqueue(dequequed.right);
			}
			System.out.print(str);
			System.out.println();
		}

	}

	public static void preOrder(BinaryTree<Integer> root){
		if(root==null)
			return;
		System.out.print(root.data+" ");
		preOrder(root.left);
		preOrder(root.right);
	}
	public static void postOrder(BinaryTree<Integer> root){
		if(root==null)
			return;
		postOrder(root.left);
		postOrder(root.right);
		System.out.print(root.data+" ");
	}

	public static void inOrder(BinaryTree<Integer> root){
		if(root==null)
			return;
		inOrder(root.left);
		System.out.print(root.data+" ");
		inOrder(root.right);
	}

	public static int getDiameter(BinaryTree<Integer> root){
		if(root==null)
			return 0;
		int d1=getDiameter(root.left);
		int d2=getDiameter(root.right);
		int d3=getHeight(root.left)+getHeight(root.right)+1;
		return Math.max(d1, Math.max(d2, d3));
	}

	public static int sumOfAllNodes(BinaryTree<Integer> root){
		if(root==null)
			return 0;
		int sum=root.data;
		sum=sum+sumOfAllNodes(root.left);
		sum=sum+sumOfAllNodes(root.right);

		return sum;
	}

	public static boolean checkStructicallySimilar(BinaryTree<Integer> root1, BinaryTree<Integer> root2){
		if(root1==null&&root2==null)
			return true;
		if(root1==null||root2==null)
			return false;

		if(root1.data!=root2.data)
			return false;

		boolean isLeft=checkStructicallySimilar(root1.left, root2.left);
		boolean isRight=checkStructicallySimilar(root1.right, root2.right);
		boolean isSame=false;

		if(isLeft==true&&isRight==true)
			isSame=true;

		else
			isSame=false;

		return isSame;

	}	
	public static void printNoSibling(BinaryTree<Integer> root){
		if(root==null)
			return;
		if(root.left!=null&&root.right!=null){
			printNoSibling(root.left);
			printNoSibling(root.right);
		}

		else if(root.left!=null){
			System.out.print(root.left.data+" ");
			printNoSibling(root.left);
		}
		else if(root.right!=null){
			System.out.print(root.right.data+" ");
			printNoSibling(root.right);
		}
	}


	public static BinaryTree<Integer> removeLeafNodes(BinaryTree<Integer> root){

		if(root.left==null&&root.right==null){
			root=null;
			return root;
		}

		if(root.left!=null){
			root.left=removeLeafNodes(root.left);
		}

		if(root.right!=null){
			root.right=removeLeafNodes(root.right);
		}

		return root;
	}

	public static int getDepth(BinaryTree<Integer> root){
		if(root.left==null&&root.right==null)
			return 0;

		int h1=Integer.MIN_VALUE;
		int h2=Integer.MIN_VALUE;
		if(root.left!=null)
			h1=getDepth(root.left);

		if(root.right!=null)
			h2=getDepth(root.right);

		return Math.max(h1, h2)+1;
	}


	public static void topOfBinaryTree(BinaryTree<Integer> root){
		if(root==null)
			return;
		printLeftBoundary(root.left);
		System.out.print(root.data+" ");
		printRightBoundary(root.right);
	}

	private static void printRightBoundary(BinaryTree<Integer> root) {
		// TODO Auto-generated method stub
		if(root==null)
			return;
		printLeftBoundary(root.right);
		System.out.print(root.data+" ");
	}

	private static void printLeftBoundary(BinaryTree<Integer> root) {
		// TODO Auto-generated method stub
		if(root==null)
			return;
		printLeftBoundary(root.left);
		System.out.print(root.data+" ");
	}

	public static boolean isBalanced(BinaryTree<Integer> root){
		int depth1=getDepth(root.left);
		int depth2=getDepth(root.right);

		boolean isBalanced;
		int diff=Math.abs(depth1-depth2);

		if(diff<=1)
			isBalanced=true;
		else
			isBalanced=false;

		return isBalanced;
	}

	public static void inOrderPrint(BinaryTree<Integer> root) throws QueueEmptyException{
		if(root==null)
			return;
		Queue<BinaryTree<Integer>> q = new Queue<BinaryTree<Integer>>();
		q.enqueue(root);
		while(true){
			int size = q.size();
			if(size==0)
				break;
			while(size>0){
				BinaryTree<Integer> dequequed=q.dequeue();
				System.out.print(dequequed.data+" ");
				if(dequequed.left!=null)
					q.enqueue(dequequed.left);
				if(dequequed.right!=null)
					q.enqueue(dequequed.right);
				size--;
			}
			System.out.println("");
		}
	}

	public static BinaryTree<Integer> createTreeFromTraversalIP(int[] pre,int[] in, int inStart, int inEnd){
		if(inStart>inEnd){
			BinaryTree<Integer> root=null;
			return root;
		}
		
		BinaryTree<Integer> root= new BinaryTree<Integer>(pre[preIndex]);
		preIndex++;
		if(inStart==inEnd)
			return root;
		int data=root.data;
		int index=search(in, inStart,inEnd,data);
		
		root.left=createTreeFromTraversalIP(pre, in, inStart, index-1);
		root.right=createTreeFromTraversalIP(pre, in, index+1, inEnd);
		
		return root;
	}

	private static int search(int[] in, int inStart, int inEnd, int data) {
		int index;
		for(index=inStart;index<=inEnd;index++){
			if(in[index]==data){
				return index;
			}
		}
		return index;
	}

	public static void main(String[] args) throws QueueEmptyException {
		BinaryTree<Integer> root=takeInput();
		//BinaryTree<Integer> root2=takeInput();
		//System.out.println(getHeight(root));
		//		preOrder(root);
		//		System.out.println();
		inOrder(root);
		//		System.out.println();
		//		postOrder(root);
		//		System.out.println(getDiameter(root));
		//System.out.println(checkStructicallySimilar(root, root2));
		//printNoSibling(root);
		//		root=removeLeafNodes(root);
		//		levelWisePrint(root);
		//System.out.println(isBalanced(root));
		//inOrderPrint(root);
//		int[] preOrder={1,2,4,5,7,3,6};
//		int[] inOrder={4,2,5,7,1,6,3};
//		BinaryTree<Integer> root2=createTreeFromTraversalIP(preOrder, inOrder, 0, inOrder.length-1);
//		levelWisePrint(root2);
	}

}
