import java.util.Scanner;


public class AverageOfBestTwo {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		String test1,test2,test3;
		double marks1,marks2,marks3,average;
		Scanner sc =new Scanner(System.in);
		System.out.print("Enter the name of the first test: ");
		test1=sc.nextLine();
		//System.out.println();
		System.out.print("Enter the name of the second test: ");
		test2=sc.nextLine();
		//System.out.println();
		System.out.print("Enter the name of the third test: ");
		test3=sc.nextLine();
		//System.out.println();
		System.out.print("Enter the marks of "+test1+": ");
		marks1=sc.nextDouble();
		//System.out.println();
		System.out.print("Enter the marks of "+test2+": ");
		marks2=sc.nextDouble();
		//System.out.println();
		System.out.print("Enter the marks of "+test3+": ");
		marks3=sc.nextDouble();
		//System.out.println();
		
		if(marks1<=marks2&&marks1<=marks3){
			average=(marks2+marks3)/2;
			System.out.println("The two best test are: "+test2+" "+test3+" with average: "+average);
		}
		else if(marks2<=marks1&&marks2<=marks3){
			average=(marks1+marks3)/2;
			System.out.println("The two best test are: "+test1+" "+test3+" with average: "+average);
		}
		else{
			average=(marks1+marks2)/2;
			System.out.println("The two best test are: "+test1+" "+test2+" with average: "+average);
		}
	}

}
