import java.util.Scanner;


public class CheckPrime {

	/**
	 * @param args
	 */
	public static boolean checkPrime(int N){
		int number=2;
		while(number<=N/2){
			if(N%number==0){
				System.out.println(N+" is not prime");
				return false;
			}
			number++;
		}
		System.out.println(N+" is prime");
		return true;
	}
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int N;
		Scanner sc= new Scanner(System.in);
		System.out.print("Enter the number to check: ");
		N=sc.nextInt();
		checkPrime(N);
	}
}