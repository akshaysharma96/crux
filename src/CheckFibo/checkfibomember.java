import java.util.*;

//Class 1 ,date: 16-01-2016

/*
    Checks whether a number is a memeber of the fibonacci series:
    
    0,1,1,2,3,5,8,13,21,35.......
*/

class CheckFiboMember{
    public static void main(String[]args){
        int number;
        Scanner sc = new Scanner(System.in);
        System.out.print("Enter number: ");
        number=sc.nextInt();
        int j=1;
        int k=0;
        int sum=0;
        while(sum<=number){
            if(sum==number){
                System.out.println(number+" is a member of the fibonacci series.");
                System.exit(0);
            }
            sum=k+j;
            k=j;
            j=sum;
        }
        System.out.println(number+" is not a member of the fibonacci series.");
    }
}