package Lecture7;

public class CheckReverse {
	
	/*
	 * Check if two strings are reverse of each other
	 * */
	
	/**
	 * @param String s1, String s2
	 * @return void
	 */
	public static boolean isReverse(String s1, String s2){
		if(s1.length()!=s2.length())
			return false;
		
		if(s1.length()==1){
			if(s1.charAt(0)==s2.charAt(0))
				return true;
			
			else
				return false;
		}
		
		int length=s2.length();
		if(s1.charAt(0)==s2.charAt(length-1))
			return isReverse(s1.substring(1), s2.substring(0,length-1));
		else 
			return false;
	}
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		String s1= "abc";
		String s2="dba";
		System.out.println(isReverse(s1, s2));
	}

}
