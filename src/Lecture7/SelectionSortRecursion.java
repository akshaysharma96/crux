package Lecture7;

public class SelectionSortRecursion {

	/**
	 * SelectionSort recursively
	 */
	
	public static void selectionSort(int a[],int firstIndex){
		if(firstIndex==a.length-1){
			return;
		}
		else{
			int key=firstIndex;
			for(int j=firstIndex+1;j<a.length;j++){
				if(a[key]>a[j]){
					key=j;
				}
			}
			int temp=a[key];
			a[key]=a[firstIndex];
			a[firstIndex]=temp;
		}
		
		selectionSort(a, firstIndex+1);
	}
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int[] a={6,31,4,5,8,9};
		selectionSort(a, 0);
		for(int i=0;i<a.length;i++){
			System.out.println(a[i]);
		}
		
	}

}
