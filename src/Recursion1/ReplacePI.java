package Recursion1;

public class ReplacePI {

	/**
	 * "abclkjpildlspi" to "abclkj3.14ldls3.14"
	 */
	public static String replacePI(String str, int index){
		if(index==str.length()){
			return "";
		}
		else if(str.charAt(index)=='p'){
			if(str.charAt(index+1)=='i'){
				return "3.14"+replacePI(str, index+2);
			}
			else{
				return String.valueOf(str.charAt(index))+replacePI(str, index+1);
			}
		}
		else{
			return String.valueOf(str.charAt(index))+replacePI(str, index+1);
		}
	}
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		String str="dfsdhpikajhapidjashpi";
		System.out.println(replacePI(str, 0));
	}

}
