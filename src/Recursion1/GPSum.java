package Recursion1;

public class GPSum {

	/**
	 * Find the sum of GP 1 + 1/2 + 1/4 + 1/8 + ......
	 * Using Recursion
	 */
	public static double sum(double k, int n , int i){
		if(i==n-1){
			return Math.pow(k, i);
		}
		else{
			return Math.pow(k, i)+sum(k, n, ++i);
		}
	}
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		double result=sum(0.5,10,0);
		System.out.println(result);
	}

}
