package Recursion1;

public class SumOfDigitsOfString {

	/**
	 * Print sum of the digits of a string using recursion
	 */
	public static int getSum(String str,int length){
		if(length==0){
			return 0;
		}
		
		if(length==1){
			return Integer.parseInt(str);
		}
		else{
			char ch =str.charAt(0);
			String s= String.valueOf(ch);
			return Integer.parseInt(s)+getSum(str.substring(1, length), length-1);
		}
	}
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.out.println(getSum("34", 2));
	}

}
