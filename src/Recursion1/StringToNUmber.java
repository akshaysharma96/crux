package Recursion1;

import java.util.Scanner;

public class StringToNUmber {

	/**
	 * "1245" to 1245
	 */
	
	public static int stringToInt(String str,int length){
		if(length==1){
			return Integer.parseInt(str);
		}
		else{
			char ch=str.charAt(0);
			String s=String.valueOf(ch);
			int value=Integer.parseInt(s);
			value=(int) (value*(Math.pow(10, length-1)));
			return value+stringToInt(str.substring(1, length), length-1);
		}
	}
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner sc = new Scanner(System.in);
		System.out.print("Enter the string: ");
		String s= sc.nextLine();
		int num=stringToInt(s, s.length());
		System.out.println("The string "+s+" in numeric form is: "+num);
	}

}
