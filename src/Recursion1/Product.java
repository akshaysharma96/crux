package Recursion1;

public class Product {

	/**
	 * Find product of 2 numbers using recursion
	 */
	
	public static int product(int m,int n){
		if(m==1){
			return n;
		}
		else{
			return n+product(m-1, n);
		}
	}
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int product=product(4, 5);
		System.out.println(product);
	}

}
