package Recursion2;

public class Keypad {

	/**
	 * @param args
	 */
	public static String[] helper(int a){

		switch(a){
		case 2 :
			return new String[]{"a","b","c"};
		
		case 3:
			return new String[]{"d","e","f"};

		case 4:
			return new String[]{"g","h","i"};
			
		case 5:
			return new String[]{"j","k","l"};
		
		case 6:
			return new String[]{"m","n","o"};
			
		case 7:
			return new String[]{"p","q","r","s"};
			
		case  8:
			return new String[]{"t","u","v"};
			
		case 9:
			return new String[]{"x","y","z"};
			
		default:
			return new String[]{""};
			
		}
	}
	public static String[] keypad(int n){
		if(n<10){
			return helper(n);
		}
		int length = (int)(Math.log10(n)+1);
		int q=n/(int)(Math.pow(10, length-1));
		int r =n%(int)(Math.pow(10, length-1));
		
		String[] out1=keypad(q);
		String[] out2=keypad(r);
		String result[]= new String[out1.length*out2.length];
		int i=0;
		for(int j=0;j<out1.length;j++){
			for(int k=0;k<out2.length;k++){
				result[i]=out1[j]+out2[k];
				i++;
			}
		}
		return result;
	}
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		String[] s=keypad(568);
		for(int i=0;i<s.length;i++){
			System.out.print(s[i]+" ");
		}
	}

}
