package Recursion2;

public class PermutationString {

	/**
	 * Print all the permutations of a string
	 */

	public static int fact(int n){
		if(n==0||n==1){
			return 1;
		}
		else{
			return n*fact(n-1);
		}
	}
	public static String[] getPermutation(String str){
		if(str.length()==1){
			return new String[]{""+str};
		}

		String[] pre=getPermutation(str.substring(1));
		char ch =str.charAt(0);
		int length=str.length();
		String[] arr = new String[fact(length)];
		int i,j,index;
		index=0;
		for(i=0;i<pre.length;i++){
			String str2=pre[i];
			for(j=0;j<str.length();j++){
				arr[index]=new StringBuilder(str2).insert(j,ch).toString();
				index++;
			}

		}
		return arr;

	}
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		String[] arr =getPermutation("kjdn");
		for(int i=0;i<arr.length;i++){
			System.out.print(arr[i]+" ");
		}
	}

}

