package Recursion2;

public class KeypadVoid {

	/**
	 * @param args
	 */
	public static String[] helper(int a){

		switch(a){
		case 2 :
			return new String[]{"a","b","c"};
		
		case 3:
			return new String[]{"d","e","f"};

		case 4:
			return new String[]{"g","h","i"};
			
		case 5:
			return new String[]{"j","k","l"};
		
		case 6:
			return new String[]{"m","n","o"};
			
		case 7:
			return new String[]{"p","q","r","s"};
			
		case  8:
			return new String[]{"t","u","v"};
			
		case 9:
			return new String[]{"x","y","z"};
			
		default:
			return new String[]{""};
			
		}
	}
	
	public static void keypadvoid(int n, String output){
		if(n==0){
			System.out.print(output +" ");
			return;
		}
		int length = (int)(Math.log10(n)+1);
		int q=n/(int)(Math.pow(10, length-1));
		int r =n%(int)(Math.pow(10, length-1));
		String[] out=helper(q);
		for(int i=0;i<out.length;i++){
			keypadvoid(r, output+out[i]);
		}
		
		
	}
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		keypadvoid(23,"");
	}

}
