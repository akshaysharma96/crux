package Recursion2;

public class StringChecker {

	/**
	 * String Checker
	 */
	public static boolean checkString(String str,boolean isStarting){
		boolean value=false;
		if(str.length()==0){
			value=false;
		}
		if(isStarting&&str.length()>=1){
			if(str.length()==1){
				if(str.equals("a"))
					return true;
				else
					return false;
			}
			else{
				value=checkString(str.substring(1), false);
			}
		}
		if(!isStarting&&str.length()>=1){
			if(str.length()==1){
				if(str.equals("a"))
					value=true;
			}
			else if(str.length()==2){
				if(str.equals("bb")||str.equals("aa"))
					value=true;
				else
					value=false;
			}

			else{
				if(str.charAt(0)=='a'&&str.subSequence(1, 3).equals("bb"))
					value=checkString(str.substring(1),false);

				else if(str.substring(0,2).equals("bb")&&str.charAt(2)=='a')
					value=checkString(str.substring(2),false);

				else if(str.substring(0,2).equals("aa"))
					value=checkString(str.substring(1), false);

				else 
					value= false;
			}
		}
		return value;
	}
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.out.println(checkString("ab", true));
	}

}
