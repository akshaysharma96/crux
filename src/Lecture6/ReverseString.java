package Lecture6;

public class ReverseString {

	/**
	 * "Hello World" to  "World Hello"
	 */
	public static void reverse(String str){
		String[] myarr=str.split(" ");
		String output="";
		for(int i=myarr.length-1;i>=0;i--){
			output=output+myarr[i];
			output=output+" ";
		}
		System.out.println(output);
	}
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		String str = "Hello  World mY NAME is Akshay";
		reverse(str);
	}

}
