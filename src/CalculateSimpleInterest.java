import java.util.Scanner;


public class CalculateSimpleInterest {
	public static void main(String[]args){
		double principleAmount, rate, time,simpleInterest;
		Scanner sc= new Scanner(System.in);
		System.out.print("Enter the principal amount: ");
		principleAmount=sc.nextDouble();
		System.out.print("Enter the rate of interest : ");
		rate=sc.nextDouble();
		System.out.print("Enter the yearly time: ");
		time=sc.nextDouble();
		simpleInterest=(principleAmount*rate*time)/100;
		System.out.println("The simple interest is: "+simpleInterest);
	}
}
