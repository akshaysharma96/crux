package Lecture5;

public class Wave {

	/**
	 * Print a wave using 2D array
	 */
	
	public static void printWave(int[][] array){
		for(int i=0;i<array.length;i++){
			if(i%2==0){
				for(int j=0;j<array[0].length;j++){
					System.out.print(array[j][i]+" ");
				}
			}
			else{
				for(int j=array[0].length-1;j>=0;j--){
					System.out.print(array[j][i]+" ");
				}
			}
		}
	}
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int a[][]={{1,2,3,4},{5,6,7,8},{9,10,11,12},{13,14,15,16}};
		printWave(a);
	}
	
	

}
