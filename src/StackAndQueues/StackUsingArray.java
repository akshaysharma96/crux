package StackAndQueues;

import L14.StackEmptyException;
import Stack.StackFullException;

public class StackUsingArray {
	private int[] stack;
	private int top=-1;

	public StackUsingArray(int length) {
		stack=new int[length];
	}

	public void push(int data) throws StackFullException{
		if(top==stack.length)
			throw new StackFullException();

		stack[++top]=data;

	}

	public int pop() throws StackEmptyException{
		if(top==-1)
			throw new StackEmptyException();
		return stack[top--];
	}

	public boolean isEmpty(){
		return top==-1;
	}

	public int size(){
		return (top+1);
	}
}
