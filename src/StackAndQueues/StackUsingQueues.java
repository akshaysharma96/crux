package StackAndQueues;

import Lecture15Queue.QueueEmptyException;
/*
 * Implement a stack using queues
 * */
public class StackUsingQueues<T> {
	private Queue<T> q1;
	private Queue<T> q2;
	
	public StackUsingQueues(){
		q1=new Queue<T>();
		q2=new Queue<T>();
	}
	
	public void push(T data){
		q1.enqueue(data);
	}
	
	public T pop() throws QueueEmptyException{
		while(q1.size()!=1){
			T data=q1.dequeue();
			q2.enqueue(data);
		}
		T returnValue=q1.dequeue();
		Queue<T> temp=q1;
		q1=q2;
		q2=temp;
		return returnValue;
	}
	
	public T top() throws QueueEmptyException{
		while(q1.size()!=1){
			T data=q1.dequeue();
			q2.enqueue(data);
		}
		T dataReturned=q1.dequeue();
		q2.enqueue(dataReturned);
		Queue<T> temp=q1;
		q1=q2;
		q2=temp;
		return dataReturned;
	}
	
	
	public static void main(String[]args){
		StackUsingQueues<Integer> sq=new StackUsingQueues<Integer>();
		for(int i=0;i<10;i++){
			sq.push(i);
		}
		
		try {
			for(int i=0;i<8;i++){
				System.out.println("Top is: "+sq.top());
				System.out.println("Popped: "+sq.pop());
			}
		} catch (QueueEmptyException e) {
			// TODO Auto-generated catch block
			System.out.println("Stack empty");
		}
	}
	
	
}
