package StackAndQueues;

public class MinStack {
	private Stack<Integer> s1;
	private Stack<Integer> s2;

	public MinStack() {
		s1 =  new Stack<Integer>();
		s2 = new Stack<Integer>();
	}
	
	public void push(int data) throws StackEmptyException{
		if(s1.isEmpty()){
			s1.push(data);
			s2.push(data);
			return;
		}
		s1.push(data);
		int topValue=s2.top();
		if(data<topValue){
			s2.push(data);
		}
	}
	
	public int pop() throws StackEmptyException{
		s2.pop();
		return s1.pop();
	}
	
	public int getMinmimum() throws StackEmptyException{
		return s2.top();
	}
	
	public int top() throws StackEmptyException{
		return s1.top();
	}
	
	public static void main(String[]args){
		MinStack m=new MinStack();
		try{
			for(int i=10;i>=0;i--)
				m.push(i);
			
			System.out.println(m.getMinmimum());
		}catch(StackEmptyException e){
			System.out.println("Catched");
		}
	}
}
