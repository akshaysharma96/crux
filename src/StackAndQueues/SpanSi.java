package StackAndQueues;

public class SpanSi {
	
	public static int[] spanSi(int[]arr) throws StackEmptyException{
		int[] result=new int[arr.length];
		
		Stack<Integer> st = new Stack<Integer>();
		st.push(0);
		result[0]=1;
		
		for(int i=1;i<arr.length;i++){
			while(!st.isEmpty()&&arr[st.top()]<arr[i]){
				st.pop();
			}
			if(st.isEmpty()){
				result[i]=i+1;
			}
			else{
				result[i]=i-st.top();
			}
			
			st.push(i);
		}
		
		return result;
	}
	
	public static void main(String[]args) throws StackEmptyException{
		int[] arr={5,8,12,7,9};
		int [] result=spanSi(arr);
		for(int i=0;i<result.length;i++){
			System.out.print(result[i]+" ");
		}
	}
}
