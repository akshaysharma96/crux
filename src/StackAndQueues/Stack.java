package StackAndQueues;

public class Stack<T> {
	private Node<T> top;
	private int size;
	
	/* Pushes element to an empty or already given stack */
	public void push(T data){
		if(top==null){
			Node<T> newNode= new Node<T>(data);
			top=newNode;
			size=1;
			return;
		}
		Node<T> newNode = new Node<T>(data);
		newNode.next=top;
		top=newNode;
		size++;
	}
	
	public T pop() throws StackEmptyException{
		if(top==null)
			throw new StackEmptyException();
	
		Node<T> nodePopped=top;
		top=top.next;
		size--;
		return nodePopped.data;
	}
	
	public T top() throws StackEmptyException{
		if(top==null)
			throw new StackEmptyException();
		return top.data;
	}
	
	public boolean isEmpty(){
		return top==null;
	}
	
	public int size(){
		return size;
	}
}
