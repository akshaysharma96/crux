package StackAndQueues;

/*Implement a queue using a stack */

public class QueueUsingStack<T> {
	private Stack<T> s1;
	private Stack<T> s2;
	
	public QueueUsingStack() {
		s1=new Stack<T>();
		s2=new Stack<T>();
	}
	
	public void enqueue(T data){
		s1.push(data);
	}
	
	public T dequeue() throws StackEmptyException{
		while(s1.size()!=1){
			T data=s1.pop();
			s2.push(data);
		}
		T returnValue=s1.pop();
		while(s2.size()!=0){
			T data=s2.pop();
			s1.push(data);
		}
		return returnValue;
	}
	
	public T front() throws StackEmptyException{
		while(s1.size()!=1){
			T data=s1.pop();
			s2.push(data);
		}
		T valueToPop=s1.pop();
		while(s2.size()!=0){
			T data=s2.pop();
			s1.push(data);
		}
		return valueToPop;
	}
	
	public static void main(String[]args){
		QueueUsingStack<Integer> qs=new QueueUsingStack<Integer>();
		for(int i=0;i<10;i++){
			qs.enqueue(i);
		}
		try {
			qs.dequeue();
			System.out.println(qs.front());
		} catch (StackEmptyException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}


